$(function() {

    // $('.swiper-container').each(function (index) {
    //     var mySwiper = new Swiper($(this)[0],{
    //         navigation: {
    //             nextEl: '.swiper-button-next',
    //             prevEl: '.swiper-button-prev',
    //         },
    //         loop: true,
    //         observer: true
    //     });
    // });

    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (isSafari) {
        $('html').addClass('_safari');
    }

    if ($('#fullpage').length) {
        $('#fullpage').fullpage({
            fixedElements: '#header, .modal-overlay',
            anchors: ['intro', 'offer-rules', 'gifts', 'products', 'offers', 'faq', 'winners', 'footer'],
            menu: '#menu',
            fitToSection: true,
            keyboardScrolling: true,
            animateAnchor: true,
            verticalCentered: false,
            normalScrollElements: '.modal-overlay',
            onLeave: function(index, nextIndex, direction) {
                var nextSection = getSectionByIndex(nextIndex);

                var sect;

                if (direction == 'down') {
                    if (!isVisible(nextSection)) {
                        sect = getSectionByIndex(index+2);
                    } else {
                        sect = getSectionByIndex(index+1);
                    }
                } else {
                    if (!isVisible(nextSection)) {
                        sect = getSectionByIndex(index-2);
                    } else {
                        sect = getSectionByIndex(index-1);
                    }
                }

                var sectId = sect.data('anchor'),
                    menu = $('.js-menu'),
                    menuEls = menu.find('li.menuanchor');

                if (sectId && menu.find('[data-menuanchor="'+ sectId +'"]').length) {
                    menuEls.removeClass('active');
                    menu.find('[data-menuanchor="'+ sectId +'"]').addClass('active');
                } else if (!menu.find('[data-menuanchor="'+ sectId +'"]').length && sectId != 'gifts2') {
                    menuEls.removeClass('active');
                }

                if(!isVisible(nextSection)) {
                    var sectionIndex = getNearestVisibleSectionIndex(nextIndex, direction);

                    if (sectionIndex !== -1) {

                        $.fn.fullpage.moveTo(sectionIndex);
                    }

                    return false;
                }

                function getSectionByIndex(index) {
                    return $('.section').eq(index - 1);
                }

                function isVisible(section) {
                    return $(section).is(':visible');
                }

                function getNearestVisibleSectionIndex(index, direction) {
                    var step = direction === 'up' ? -1 : 1,
                        sectionsCount = $('.section').length;

                    while (index >=0 && index < sectionsCount && !isVisible(getSectionByIndex(index))) {
                        index += step;
                    }

                    return index;
                }
            }
        });
    };

    var menuGrade = function() {
        if ($('.js-step').length) {
            var menuItems = $('.js-step');
            var step = -13.2;
            if ($(window).width() < 640) {
                var step = -6.6;
            }
            $.each(menuItems, function() {
                var index = $(this).attr('data-index');
                var coord = index * step;
                $(this).css('left', coord);
            })
        }
    };

    $('.js-submenu-trigger').hover(
        function() {
            $(this).addClass('expanded');
            $(this).find('.js-submenu').addClass('show');
        },
        function() {
            $(this).removeClass('expanded');
            $(this).find('.js-submenu').removeClass('show');
        }
    );

    $('.js-special-products-trigger').on('click', function() {
        $(this).closest('.js-special-products').toggleClass('expanded');
    });

    if ($('.scrollable').length) {
        $('.scrollable').mCustomScrollbar();
    };

    if ($('._date').length) {
        $("._date").mask("99/99/9999",{placeholder:"ДД/ММ/ГГГГ"});
    }

    if ($('.js-tabs-rtl').length) {
        $('.js-tab-control').on('click', function() {
            var n = $('.js-tab-control').length - 1 - $(this).index();
            $('.js-tab-control').removeClass('active');
            $(this).addClass('active');
            $('.js-tab').removeClass('active');
            $.each($('.js-tab'), function() {
                if ($(this).index() === n) {
                    $(this).addClass('active');
                }
            });

            if ($(window).width() < 768) {
                var src = $(this).find('img').attr('src'),
                    imgWidth = $(this).find('img').attr('width'),
                    imgHeight = $(this).find('img').attr('height');
                $('.js-tab-indicator').attr('src', src);
                $('.js-tab-indicator').attr('width', imgWidth);
                $('.js-tab-indicator').attr('height', imgHeight);
                $(this).closest('.js-tab-controls-wrapper').removeClass('opened');
            }
        })
    };

    if ($('.js-question-trigger').length) {
        $('.js-question-trigger').on('click', function() {
            var l = $(this).closest('.js-question').index(),
                _this = $(this);
            $.each(_this.parents('.faq__col').find('.js-question'), function() {
                if ($(this).index() === l) {
                    $('.js-question').removeClass('expanded');
                    $(this).toggleClass('expanded');
                } else {
                    $(this).removeClass('expanded');
                }
            });
        })
    };

    if ($('.js-sorting-link').length) {
        $('.js-sorting-link').on('click', function() {
            $(this).toggleClass('reverse');
        })
    };

    $('.js-menu-trigger').on('click', function() {
        $('.js-page-header').toggleClass('opened');
    });

    $('.submenu__link').on('click', function() {
        if ($(window).width() < 1024) {
            $('.js-page-header').removeClass('opened');
        }
    });

    if ($('.js-mobile-slider').length && $(window).width() < 768) {
        $('.js-mobile-slider').addClass('swiper-container');
        $('.js-mobile-wrapper').addClass('swiper-wrapper');
        $('.js-mobile-slide').addClass('swiper-slide');

        var mobileSlider1 = new Swiper('.js-gifts-slider', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            observer: true
        });

        var mobileSlider2 = new Swiper('.js-products-slider', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            observer: true
        })
    };

    var modalPosition = function() {
        if ($('.js-modal').length) {
            var modalEls = $('.js-modal .modal');
            $.each(modalEls, function() {
                if ($(window).width() > 639) {
                    if ($(this).outerHeight() > $(window).height()) {
                        var margin = $(this).outerHeight() / 2;
                        $(this).css('top', '0').css('margin-top', margin);
                    } else {
                        $(this).css('top', '50%').css('margin-top', '0');
                    }
                }
            })
        }
    };

    $('.js-tab-controls-button').on('click', function() {
        var tabControlsWidth = $(this).outerWidth();
        $(this).closest('.js-tab-controls-wrapper').find('.tab-controls').css('width', tabControlsWidth);
        $(this).closest('.js-tab-controls-wrapper').toggleClass('opened');
    });

    if ($(window).width() < 768 && $('.total-winners').length) {
        var winnersQty = $('.winners-table__row').length;
        $('.total-winners').text(winnersQty);
    }

    $('.js-winner-btn').on('click', function() {
        var winners = $('.winners-table__row');
        var winnerActiveIndex = $('.winners-table__row.active').index();
        if ($(this).hasClass('prev')) {
            if (winnerActiveIndex > 1) {
                winnerActiveIndex--;
            } else {
                winnerActiveIndex = winnerActiveIndex;
                $('.js-winner-btn').removeClass('disabled');
                $(this).addClass('disabled');
            }
        } else {
            if (winnerActiveIndex < (winners.length - 1)) {
                winnerActiveIndex++;
            } else {
                winnerActiveIndex = winnerActiveIndex;
                $('.js-winner-btn').removeClass('disabled');
                $(this).addClass('disabled');
            }
        };
        winners.removeClass('active')
        $.each(winners, function() {
            if ($(this).index() === winnerActiveIndex) {
                $(this).addClass('active');
            }
        });
        $('.current-winner').text(winnerActiveIndex);
    });

    if ($('.custom-select').length) {
        $('.custom-select').selectric();
    }

    modalPosition();
    menuGrade();

    $(window).on('resize', function() {
        modalPosition();
        menuGrade();
    });

    $("input:file.form__upload").change(function (){
        var n;

        if ($(this).val().lastIndexOf('\\')) {
            n = $(this).val().lastIndexOf('\\') + 1;
        } else {
            n = $(this).val().lastIndexOf('/') + 1;
        }
        var fileName = $(this).val().slice(n);
        $(".form__upload-block").find('.path').html(fileName);
    });

    var codeBtn = $('.js-code-btn');

    codeBtn.on('click', function () {
        var form = $(this).parent().find('.reg-code__inner');

        $(this).addClass('_hidden');
        form.addClass('_show');

        return false;
    })
});










